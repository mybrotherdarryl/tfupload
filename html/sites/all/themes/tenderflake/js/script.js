/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document) {

  'use strict';

	$( window ).resize( function(){ 
		$('.views_slideshow_cycle_main').each(function () {
			var img_height;
			$(this).find('.views-slideshow-cycle-main-frame-row').each(function () {
				var tmp_img_height = $(this).height();
				if (tmp_img_height !== 0 ) {
					img_height = tmp_img_height;
				}
				return;
			});
			if (img_height !== 0) {
				$(this).height(img_height).children('.views-slideshow-cycle-main-frame').height(img_height);
			}
			return;
		});
	});
  
	$(document).ready( function() {
		//console.log( "ready!" );
		//loadPlayer();
		$('.views_slideshow_cycle_main').each(function () {
			var img_height;
			$(this).find('.views-slideshow-cycle-main-frame-row').each(function () {
				var tmp_img_height = $(this).height();
				if (tmp_img_height !== 0 ) {
					img_height = tmp_img_height;
				}
				return;
			});
			if (img_height !== 0) {
				$(this).height(img_height).children('.views-slideshow-cycle-main-frame').height(img_height);
			}
			return;
		});		
	});
  
	// embedding youtube iframe api
	// https://developers.google.com/youtube/iframe_api_reference#Getting_Started
	/*
	function loadPlayer() { 
	  if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {

		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		window.onYouTubePlayerAPIReady = function() {
		  //////console.log('line1');
		  onYouTubePlayer();
		};

	  } else {
		////console.log('line2');
		onYouTubePlayer();
	  }
	}

	var player;
	function onYouTubePlayer() {
		player = new YT.Player('media-youtube-vaw4a-w7mjc', {
			playerVars : {
				'autoplay' : 1,
				'rel' : 0,
				'showinfo' : 0,
				'showsearch' : 0,
				'controls' : 0,
				'loop' : 1,
				'enablejsapi' : 1,
				//'playlist': 'your-single-video-ID'
			},		
			events: {
				'onReady': onPlayerReady,
				'onStateChange': onPlayerStateChange
			}
		});
		////console.log('set player');
	}  

	function onPlayerReady() {
		////console.log('a');
		player.stopVideo();
		player.playVideo();
		////console.log('mute');
		// Mute!
		player.mute();
		var video_duration = player.getDuration();
		//console.log(video_duration);
		////console.log('pausing');
		Drupal.viewsSlideshow.action({ "action": 'pause', "slideshowID": "homepage_background_image_slider-block_1", "force": true });
	}
	
	function playsound() {
		player.unMute();
	}
	
	var done = false;
	function onPlayerStateChange(event) {
		////console.log('state is called');
		console.log(event.data);
		/*
		if (event.data == YT.PlayerState.PLAYING && !done) {
			////console.log('slide');
		  setTimeout(function() {
			Drupal.viewsSlideshow.action({ "action": 'play', "slideshowID": "homepage_background_image_slider-block_1", "force": true });
		  }, 14500);
		  done = true;
		}*/
		/*
		if (event.data == YT.PlayerState.ENDED) {
			////console.log('end');
			Drupal.viewsSlideshow.action({ "action": 'play', "slideshowID": "homepage_background_image_slider-block_1", "force": true });
			//player.stopVideo();		
		}
	} */	
	/*
	$('.video-sound').click(function(){
		playsound();
	});
	*/	

  // To understand behaviors, see https://drupal.org/node/756722#behaviors
  Drupal.behaviors.my_custom_behavior = {
    attach: function (context, settings) {
	//////console.log('run');
	$(window).load(function() {
		$('.views-slideshow-cycle-main-frame').each(function(){
			//console.log($(this).height());
			$(this).find('.views-slideshow-cycle-main-frame-row').css('height', $(this).height());
		});
		$('.owl-carousel .owl-nav .owl-prev, .owl-carousel .owl-nav .owl-next').css('height',$('.view-display-id-attachment_1 .owl-stage').height()+'px');		
	});
	
	$('.i18n-en #views-exposed-form-all-recipes-page #edit-combine, .i18n-en #views-exposed-form-flag-bookmarks-page #edit-combine').attr('placeholder','Search All Recipes');
	$('.i18n-fr #views-exposed-form-all-recipes-page #edit-combine, .i18n-fr #views-exposed-form-flag-bookmarks-page #edit-combine').attr('placeholder','Chercher toutes les recettes');
	$('.view-id-all_recipes .views-fluid-grid-list .views-fluid-grid-inline, .view-id-related_items .view-content .views-fluid-grid-inline, .view-id-flag_bookmarks .views-fluid-grid-list .views-fluid-grid-inline').each(function(){
		//console.log($(this).find('.views-field-title a').attr('href'));
		$(this).find('.file').wrap('<a href="'+$(this).find('.views-field-title a').attr('href')+'"></a>');
	});
	
	$('.not-logged-in.i18n-en .node-recipe .group-recipe-title .field-name-field-favourite .fivestar-default').append('<div class="tooltip"><span class="tooltiptext">Login to rate</span></div>');
	$('.not-logged-in.i18n-fr .node-recipe .group-recipe-title .field-name-field-favourite .fivestar-default').append('<div class="tooltip"><span class="tooltiptext">Ouvrir une session pour donner une note</span></div>');
	$('.i18n-en #user-login-form .form-submit').val('LOGIN');
    }
  };
  
	$(document).ready(function() {
		$('.i18n-en #user-login-form .form-submit').val('LOGIN');
		$('.owl-carousel .owl-nav .owl-prev, .owl-carousel .owl-nav .owl-next').height(0);
		if($('.owl-carousel').find('div').hasClass('item-1')) {
			//////console.log('more than one');
		} else {
			//////console.log('only one');
			$('.owl-carousel').css('margin-bottom', '0');
			$('.owl-carousel').addClass('slidefull');
			$('.attachment-after .view-display-id-attachment_1').hide();
		}
		
		$('.node-recipe .field-name-field-recipe-ingredients .field-item .field-collection-view .field-name-field-ingredient-list .field-item').click(function(){
			//////console.log('click1');
			$(this).toggleClass('checked');
		});
		
		$('.flag-bookmarks.flag-non-login').hover(
		function(){
			if($('body').hasClass('i18n-en')){
				$('.tooltip.en .tooltiptext').css('visibility','visible');
			} else {
				$('.tooltip.fr .tooltiptext').css('visibility','visible');
			}
		},
		function(){
			$('.flag-bookmarks .tooltip .tooltiptext').css('visibility','hidden');
		});
		/*
		if($('body').hasClass('node-type-recipe') || $('body').hasClass('node-type-products')) {
			
		var $sync1 = $("#owl-carousel-block10, #owl-carousel-block11"), // This is the ID of the block with the single item/big image. It's possible to use ID's of multiple slideshows, separated by a comma <- Make sure this is the ID of the div that also has the class "owl-carousel"
			$sync2 = $("#owl-carousel-attachment-110, #owl-carousel-attachment-111"), // This is the ID of the attachment with multiple items. It's possible to use ID's of multiple carousels, separated by a comma <- Make sure this is the ID of the div that also has the class "owl-carousel"
			flag = false,
			duration = 300;

			$sync1.owlCarousel({
				// these group settings are configured with Owl's UI in Drupal, although "item:1" may be required to work on Android's native browser
			  })
			  .on('changed.owl.carousel', function (e) {
				if (!flag) {
				  flag = true;
				  $sync2.trigger('to.owl.carousel', [e.item.index, duration, true]);
				  flag = false;
				}
			  });

			  $sync2.owlCarousel({
					// these group settings are configured with Owl's UI in Drupal
				  })
				  .on('click', '.owl-item', function () {
					  ////console.log('click');
					  console.log($(this).children(':first').attr('class'));
					  if($(this).find('div').hasClass('file-video')) {
						////console.log('youtube video');
					  } else {
						$('#media-youtube-r7nfumsvsoy').stopVideo();
					  }
					$sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);

				  })
				  .on('changed.owl.carousel', function (e) {
					if (!flag) {
					  flag = true;
					  $sync1.trigger('to.owl.carousel', [e.item.index, duration, true]);
					  flag = false;
					}
				  });
		}*/
		
		$('.page-user-password.i18n-fr h1').text('Nouveau mot de passe requis');
	});
	
	var getUrlParameter = function getUrlParameter(sParam) {
		var sPageURL = decodeURIComponent(window.location.search.substring(1)),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;

		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : sParameterName[1];
			}
		}
	};		
	
	$(window).load(function(){
		var rc = getUrlParameter('sort_by');
		if(rc) {
			$('html, body').stop().animate({
				scrollTop: $('#block-block-12').offset().top - 0
			}, 500);			
		}		
	});
  
})(jQuery, Drupal, this, this.document);
