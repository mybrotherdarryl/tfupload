<?php
/**
 * @file
 * Returns the HTML for a node.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728164
 */
 global $base_url;
?>
<article class="<?php print $classes; ?> clearfix node-<?php print $node->nid; ?>"<?php print $attributes; ?>>

  <?php if ($title_prefix || $title_suffix || $display_submitted || $unpublished || $preview || !$page && $title): ?>
    <header>
      <?php if ($display_submitted): ?>
        <p class="submitted">
          <?php print $user_picture; ?>
          <?php print $submitted; ?>
        </p>
      <?php endif; ?>

      <?php if ($unpublished): ?>
        <mark class="watermark"><?php print t('Unpublished'); ?></mark>
      <?php elseif ($preview): ?>
        <mark class="watermark"><?php print t('Preview'); ?></mark>
      <?php endif; ?>
    </header>
  <?php endif; ?>

  <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);	
  ?>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>

</article>
<?php
	$current_nid = $node->nid;
	global $user;
	global $language ;
	$lang_name = $language->language ;
	$cb = flag_get_user_flags('node',$current_nid,$user->uid);
	
	if($cb['bookmarks']->entity_id == $current_nid) {
		$block = module_invoke('webform', 'block_view', 'client-block-56');
		print "<div class=\"block block-notes\">";
		print render($block['content']);
		print "</div>";
	}
	//print "<pre>";
	//print $node->lang;
	//print_r($node->field_recipe_ingredients);
	
	$item_id = $node->field_recipe_ingredients[$lang_name][0]['value'];
	$ingredients = entity_load('field_collection_item', array($item_id));
	//print_r($ingredients);
	//print "</pre>";
?>
<meta property="og:site_name" content="<?php print $base_url;?>" />

<div itemscope itemtype="http://schema.org/Recipe" style="display:none;">
    <h1 itemprop="name"><?php print $node->title;?></h1>
    <meta itemprop="url" content="http://myrecipesite.com/pineapple.html" />
    <span itemprop="totalTime">15 mins</span>
    <span itemprop="recipeYield">Serves 2</span>
    Ingredients:
        <span itemprop="ingredients">1 Naan bread</span>,
        <span itemprop="ingredients">3 pieces fresh mozzarella</span>,
        <span itemprop="ingredients">1 1/2 tbsp olive oil</span>,
        <span itemprop="ingredients">1 1/2 tbsp balsamic vinegar</span>,
        <span itemprop="ingredients">5 basil leaves</span>
        <span itemprop="ingredients">3 cloves garlic</span>
        <span itemprop="ingredients">1 tomato</span>
</div>