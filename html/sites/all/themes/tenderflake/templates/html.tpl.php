<?php
/**
 * @file
 * Returns the HTML for the basic html structure of a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728208
 */
 global $user;
?><!DOCTYPE html>
<html <?php print $html_attributes . $rdf_namespaces; ?>>
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>

  <?php if ($default_mobile_metatags): ?>
    <meta name="MobileOptimized" content="width">
    <meta name="HandheldFriendly" content="true">
    <meta name="viewport" content="width=device-width">
  <?php endif; ?>

  <?php print $styles; ?>
  <?php print $scripts; ?>
  <?php if ($add_html5_shim): ?>
    <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_zen; ?>/js/html5shiv.min.js"></script>
    <![endif]-->
  <?php endif; ?>
  <script src="https://www.youtube.com/iframe_api"></script>
<!-- FIX FACEBOOK LOGIN JUNK URL ----->
<script type="text/javascript">
if (window.location.hash && window.location.hash == '#_=_') {
window.location.hash = '';
}
</script>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <?php if ($skip_link_text && $skip_link_anchor): ?>
    <p class="skip-link__wrapper">
      <a href="#<?php print $skip_link_anchor; ?>" class="skip-link visually-hidden visually-hidden--focusable" id="skip-link"><?php print $skip_link_text; ?></a>
    </p>
  <?php endif; ?>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
</body>
</html>
<script src="https://www.youtube.com/iframe_api"></script>
<script src="https://www.youtube.com/player_api"></script>
<?php if(arg(0)=='home') {?>
<script>
(function ($, Drupal, window, document) {

	slidenum = $('.view-homepage-background-image-slider .views-slideshow-cycle-main-frame-row-item').length;
	console.log('slide num : ' + slidenum);
	
	if(slidenum==1) {
		$('.views-slideshow-controls-bottom').hide();
	}
	
	var played = 0;
	var player;
	var video_duration = 0;
	var firsttime = 0;
	var videoid = $('.media-youtube-player').attr('id');
	
	function loadPlayer() {
	  if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {

		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		window.onYouTubePlayerAPIReady = function() {
		  console.log('yt on ready');
		  onYouTubePlayer(videoid);
		};

	  } else {
		console.log('yt repeat');
		//document.getElementById('media-youtube-vaw4a-w7mjc').contentDocument.location.reload(true);
		//var iframe = document.getElementById(videoid);
		//iframe.src = iframe.src;
		//onPlayerStateChange(1);
		onYouTubePlayer(videoid);
	  }
	}
	
	function onYouTubePlayer() {
		console.log('playing video: '+videoid);
		// console.log($('#'+videoid).attr('src'));
		// media-youtube-hagtvgyzwty
		$('#'+videoid).attr('src',$('#'+videoid).attr('src'));
		//document.getElementsByTagName('iframe')[0].src=document.getElementsByTagName('iframe')[0].src;
		player = new YT.Player(videoid, {
			playerVars : {
				'autoplay' : 1,
				'rel' : 0,
				'showinfo' : 0,
				'showsearch' : 0,
				'controls' : 0,
				'loop' : 1,
				'enablejsapi' : 1,
				//'playlist': 'your-single-video-ID'
			},		
			events: {
				'onReady': onPlayerReady,
				'onStateChange': onPlayerStateChange
			}
		});
	}  

	function onPlayerReady() {
		console.log('player ready');
		// Mute!
		player.mute();
		player.stopVideo();
		player.playVideo();
		video_duration = player.getDuration() * 1000;
		if(slidenum>1) {
			console.log('pausing');
			Drupal.viewsSlideshow.action({ "action": 'pause', "slideshowID": "homepage_background_image_slider-block_1", "force": true });
		}
	}
	
	function playsound() {
		player.unMute();
	}
	
	var done = false;
	function onPlayerStateChange(event) {
		//console.log('state is called');
		//console.log(event.data);
		//console.log(video_duration);
		if (event.data == YT.PlayerState.PLAYING || event == YT.PlayerState.PLAYING) {
			console.log('state running');
			newtimer = video_duration - 4000;
			console.log('newtimer:'+newtimer);
			Drupal.viewsSlideshow.action({ "action": 'pause', "slideshowID": "homepage_background_image_slider-block_1", "force": true });
			setTimeout(function() {
				console.log('time to slide!');
				Drupal.viewsSlideshow.action({ "action": 'play', "slideshowID": "homepage_background_image_slider-block_1", "force": true });
				firsttime = 1;
			}, newtimer);
			//done = true;
		}
		if (event.data == YT.PlayerState.ENDED) {
			console.log('yt end playing');
			//player.stopVideo();
			//if(played==1) {
				//console.log('repeat');
				//Drupal.viewsSlideshow.action({ "action": 'play', "slideshowID": "homepage_background_image_slider-block_1", "force": true });
			//}
		}
		
	}
		
    if($('body').hasClass('front')) {
		
		Drupal.viewsSlideshowPager.transitionBegin = function (options) {
			console.log('sliding');
			//console.log(options.slideNum);
			$('.views-slideshow-pager-field-item').removeClass('active');
			$('#views_slideshow_pager_field_item_bottom_homepage_background_image_slider-block_1_'+options.slideNum).addClass('active');
			if(firsttime==1) {
				if($('#views_slideshow_cycle_div_homepage_background_image_slider-block_1_'+options.slideNum).find('div').hasClass('file-video')) {
					console.log('video type');
					loadPlayer();
				} else {
					console.log('image type');
				}
			}
		};
		
	}
	
	Drupal.viewsSlideshow.action({ "action": 'pause', "slideshowID": "homepage_background_image_slider-block_1", "force": true });
	loadPlayer();

})(jQuery, Drupal, this, this.document);
</script>
<?php }
$nodecheck = node_load(arg(1));
if($nodecheck->type=='recipe'||$nodecheck->type=='products') {
?>
<script>
(function ($, Drupal, window, document) {

	$(document).ready(function() {
		
		if($('body').hasClass('node-type-recipe') || $('body').hasClass('node-type-products')) {
			
		var $sync1 = $("#owl-carousel-block10, #owl-carousel-block11"), // This is the ID of the block with the single item/big image. It's possible to use ID's of multiple slideshows, separated by a comma <- Make sure this is the ID of the div that also has the class "owl-carousel"
			$sync2 = $("#owl-carousel-attachment-110, #owl-carousel-attachment-111"), // This is the ID of the attachment with multiple items. It's possible to use ID's of multiple carousels, separated by a comma <- Make sure this is the ID of the div that also has the class "owl-carousel"
			flag = false,
			duration = 300;

			$sync1.owlCarousel({
				// these group settings are configured with Owl's UI in Drupal, although "item:1" may be required to work on Android's native browser
			  })
			  .on('changed.owl.carousel', function (e) {
				if (!flag) {
				  flag = true;
				  $sync2.trigger('to.owl.carousel', [e.item.index, duration, true]);
				  flag = false;
				}
			  });

			  $sync2.owlCarousel({
					// these group settings are configured with Owl's UI in Drupal
				  })
				  .on('click', '.owl-item', function () {
					  //console.log('click');
					  //console.log($(this).children(':first').attr('class'));
					  if($(this).find('div').hasClass('file-video')) {
						//console.log('youtube video');
					  } else {
						  //console.log('no video');
						  if($('.media-youtube-player').length) {
							player1.stopVideo();
						  }
					  }
					$sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);

				  })
				  .on('changed.owl.carousel', function (e) {
					if (!flag) {
					  flag = true;
					  $sync1.trigger('to.owl.carousel', [e.item.index, duration, true]);
					  flag = false;
					}
				  });
		}
	});

})(jQuery, Drupal, this, this.document);
</script>
<?php } ?>