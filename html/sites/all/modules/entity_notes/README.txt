Welcome to Entity Notes.

-- SUMMARY --

Entity Notes allows you to add a note to any Entities on the site.

A note is a simple way to add information to any entity. Notes can 
be used differently for different cases. Notes are a general way 
for a single individual or a group to share information about an o
bject directly on the website.

-- REQUIREMENTS --

Views 3


-- INSTALLATION --

1) Install as usual, see http://drupal.org/node/70151 for further information.

2) Navigate to the configuration page: admin/config/content/entity_notes/settings

3) Set the proper permissions for the roles you want.
   - Unless you give "access" permission, the user will not be allow to see the note tab.
   

-- IMPORTANT NOTE--

Do not remove any of the Contextual filters in the Entity Note views.

Required contextual filters are:
* Note: Entity Type
* Note: Bundle
* Note: Entity ID
These Contextual filters must stay in the same order.
