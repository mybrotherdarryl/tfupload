<?php

/**
 * @file
 * Provide a note entity id argument handler.
 */

/**
 * Argument handler to accept an entity_id.
 */
class entity_notes_handler_argument_note_entity_id extends views_handler_argument_numeric {

}
