<?php

/**
 * @file
 * Provide a note body field for the field handlers.
 */

/**
 * Field handler to provide a body field
 */
class entity_notes_handler_field_body extends views_handler_field {

  function render($values) {
    $value = $this->get_value($values);
    return nl2br($value);
  }
}