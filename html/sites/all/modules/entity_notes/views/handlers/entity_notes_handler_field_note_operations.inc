<?php

/**
 * @file
 * Provide a note operations field for the field handlers.
 */

/**
 * Field handler to present an note's operations links.
 */
class entity_notes_handler_field_note_operations extends views_handler_field {
  function construct() {
    parent::construct();

    $this->additional_fields['note_id']     = 'note_id';
    $this->additional_fields['entity_type'] = 'entity_type';
    $this->additional_fields['bundle']      = 'bundle';
    $this->additional_fields['entity_id']   = 'entity_id';
    $this->additional_fields['uid']         = 'uid';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $note_id      = $this->get_value($values, 'note_id');
    $entity_type  = $this->get_value($values, 'entity_type');
    $bundle_name  = $this->get_value($values, 'bundle');
    $entity_id    = $this->get_value($values, 'entity_id');
    $uid          = $this->get_value($values, 'uid');

    $entity = entity_note_get_entities(array('entity_type' => $entity_type, 'bundle' => $bundle_name));
    $url_path = entity_notes_get_url_path($entity[$entity_type]['bundles'][$bundle_name]['path'], $entity_id);

    $operations = array();

    //Check if user has access to delete this note
    if (entity_notes_access($entity_type, $bundle_name, $entity_id, 'edit', $uid, $note_id)) {
      $operations[] = l('<img src="/' . drupal_get_path('module', 'entity_notes') . '/images/edit.png" height="16" width="16" alt="' . t('Edit') . '" title="' . t('Edit') . '" />', $url_path['path'] . "/entity_notes/{$note_id}/edit", array('html' => TRUE));
    }
    //Check if user has access to modify this note
    if (entity_notes_access($entity_type, $bundle_name, $entity_id, 'delete', $uid, $note_id)) {
      $operations[] =  l('<img src="/' . drupal_get_path('module', 'entity_notes') . '/images/delete.png" height="16" width="16" alt="' . t('Delete') . '" title="' . t('Delete') . '" />', $url_path['path'] . "/entity_notes/{$note_id}/delete", array('html' => TRUE));
    }

    $links = implode("&nbsp;&nbsp;" , $operations);

    return $links;
  }
}
