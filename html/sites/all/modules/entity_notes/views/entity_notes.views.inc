<?php

/**
 * @file
 * Provide views data and handlers for entity_notes.module
 */

/**
 * Implements hook_views_data().
 */
function entity_notes_views_data() {
  $data = array();

  $data['entity_notes']['table']['group']  = t('Note');

  $data['entity_notes']['table']['base'] = array(
    'field' => 'note_id',
    'title' => t('Note'),
    'help' => t('Notes added to entities.'),
    'entity type' => 'note',
    'access query tag' => 'entity_notes_access',
  );

  // Expose the note ID.
  $data['entity_notes']['note_id'] = array(
    'title' => t('Note ID'),
    'help' => t('The unique internal identifier of the note.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_field_numeric',
    ),
  );

  // Expose the note entity type.
  $data['entity_notes']['entity_type'] = array(
    'title' => t('Entity Type'),
    'help' => t('The entity type that the note referance to.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose the note entity bundle.
  $data['entity_notes']['bundle'] = array(
    'title' => t('Bundle'),
    'help' => t('The bundle that the note referance to.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose the note entity ID.
  $data['entity_notes']['entity_id'] = array(
    'title' => t('Entity ID'),
    'help' => t('The entity id that the note referance to.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_field_numeric',
      'handler' => 'entity_notes_handler_argument_note_entity_id',
      'name field' => 'entity_id',
      'numeric' => TRUE,
      'validate type' => 'entity_id',
    ),
  );

  // Expose the note owner's user id.
  $data['entity_notes']['uid'] = array(
    'title' => t('Uid'),
    'help' => t("The owner's user ID."),
    'field' => array(
      'handler' => 'entity_notes_handler_field_user',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'relationship' => array(
      'title' => t('Owner'),
      'help' => t("Relate this note to its owner's user account"),
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'field' => 'uid',
      'label' => t('Note owner'),
    ),
  );

  // Expose the note body content.
  $data['entity_notes']['body'] = array(
    'title' => t('Body'),
    'help' => t('The body of the note.'),
    'field' => array(
      'handler' => 'entity_notes_handler_field_body',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Expose the note's creation date.
  $data['entity_notes']['created'] = array(
    'title' => t('Post date'),
    'help' => t('The date the note was posted.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );

  // Expose the note's modification date.
  $data['entity_notes']['modified'] = array(
    'title' => t('Updated date'),
    'help' => t('The date the note was last updated.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
  );

  $data['entity_notes']['operations'] = array(
    'field' => array(
      'title' => t('Operations links'),
      'help' => t('Display all the available operations links for the note.'),
      'handler' => 'entity_notes_handler_field_note_operations',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_query_alter().
 */
function entity_notes_views_query_alter(&$view, &$query) {
  global $user;

  if ($view->name == 'entity_notes') {
    if (!empty($view->args[0]) && !$view->args[1]) {
      $entity_type = $view->args[0];
      $bundle_name = $view->args[1];

      if (user_access("view any entity notes") || user_access("view any {$entity_type} {$bundle_name} entity notes")) {
        return;
      }

      elseif (user_access("view own entity notes")) {
        $view->query->add_where('0', "entity_notes.uid", $user->uid);
      }

      elseif (user_access("view own {$entity_type} {$bundle_name} entity notes")) {
        $view->query->add_where('0', "entity_notes.uid", $user->uid);
      }

      //If the user doesn't have any permissions then set uid to -1 to return no results.
      else{
        $view->query->add_where('0', "entity_notes.uid", -1);
      }
    }
  }
}
