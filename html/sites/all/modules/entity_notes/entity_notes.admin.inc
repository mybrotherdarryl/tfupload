<?php

/**
 * @file
 * Admin page callbacks for the entity notes module.
 */
function entity_notes_overview_form($form, &$form_state) {

  //Include the css file
  drupal_add_css(drupal_get_path('module', 'entity_notes') . '/entity_notes.admin.css');

  //Get default values
  $entities = entity_note_get_entities();

  $form = array();
  $form['entities'] = array(
    '#tree' => TRUE,
  );
  $form['#entities_info'] = entity_get_info();
  foreach ($form['#entities_info'] as $entity_type => $entity_info) {
    if ($entity_type == "note") continue; //Skip the note entity

    $form['entities'][$entity_type] = array(
      '#type' => 'fieldset',
      '#title' => filter_xss($entity_info['label']),
    );

    $help = "In your path you must include the token %entity_id where the id of your entity should be.";
    $form['entities'][$entity_type]['help'] = array(
      '#markup' => $help,
      '#prefix' => '<div class="entity-note-desc">',
      '#suffix' => '</div>',
    );

    $form['entities'][$entity_type]['bundles'] = array(
      '#type' => 'container',
      '#theme' => 'table',
      '#header' => array(t('Enable'), t('Bundle Name'), t('Bundle Path')),
      '#rows' => array(),
    );

    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
      $form['entities'][$entity_type]['bundles'][$bundle_name]['enable'] = array(
        '#type' => 'checkbox',
        '#default_value' => isset($entities[$entity_type]['bundles'][$bundle_name]) ? TRUE : FALSE,
      );
      $form['entities'][$entity_type]['bundles'][$bundle_name]['path'] = array(
        '#type' => 'textfield',
        '#default_value' => !empty($entities[$entity_type]['bundles'][$bundle_name]['path']) ? $entities[$entity_type]['bundles'][$bundle_name]['path'] : '',
      );

      $form['entities'][$entity_type]['bundles']['#rows'][] = array(
        array(
          'data' => array(
            &$form['entities'][$entity_type]['bundles'][$bundle_name]['enable']
          ),
          'width' => '30',
        ),
        array(
          'data' => array(
            '#markup' => $bundle_info['label'],
          ),
          'width' => '300',
        ),
        array(
          'data' => array(
            &$form['entities'][$entity_type]['bundles'][$bundle_name]['path']
          ),
        ),
      );
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );

  return $form;
}

/**
 * Form Validation callback for Admin Form
 */
function entity_notes_overview_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $entity_get_info = $form['#entities_info'];

  foreach ($values['entities'] as $entity_type => $entity_info) {
    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
      if ($bundle_info['enable'] && empty($bundle_info['path'])) {
        form_error($form['entities'][$entity_type]['bundles'][$bundle_name]['path'], t('Bundle Path is required for the !bundle_name bundle.', array('!bundle_name' => t($entity_get_info[$entity_type]['bundles'][$bundle_name]['label']))));
      }
      elseif ($bundle_info['enable'] && !empty($bundle_info['path']) && (strpos($bundle_info['path'], "%entity_id") === FALSE)) {
        form_error($form['entities'][$entity_type]['bundles'][$bundle_name]['path'], t('!token must be included in the Bundle Path for the !bundle_name bundle.', array('!bundle_name' => t($entity_get_info[$entity_type]['bundles'][$bundle_name]['label']), '!token' => "%entity_id")));
      }
      elseif ($bundle_info['enable'] && !empty($bundle_info['path']) && substr(trim($bundle_info['path']), -1) == "/") {
        form_error($form['entities'][$entity_type]['bundles'][$bundle_name]['path'], t('Bundle Path cannot end with a trailing slash for the !bundle_name bundle.', array('!bundle_name' => t($entity_get_info[$entity_type]['bundles'][$bundle_name]['label']), '!token' => "%entity_id")));
      }
    }
  }
}

/**
 * Form submit callback for Admin Form
 */
function entity_notes_overview_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  foreach ($values['entities'] as $entity_type => $entity_info) {
    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {

      $count = db_query("SELECT COUNT(entity_type) FROM {entity_notes_entities} WHERE entity_type = :entity_type AND bundle = :bundle", array(':entity_type' => $entity_type, ':bundle' => $bundle_name))->fetchField();

      //Delete an exsisting entity
      if (!$bundle_info['enable'] && $count > 0) {
        db_delete("entity_notes_entities")
          ->condition('entity_type', $entity_type)
          ->condition('bundle', $bundle_name)
          ->execute();
        continue;
      }

      if ($bundle_info['enable']) {
        $record = array(
          'entity_type' => $entity_type,
          'bundle' => $bundle_name,
          'path' => $bundle_info['path'],
        );

        //Update entity
        if ($count > 0) {
          $keys = array('entity_type', 'bundle');
          drupal_write_record('entity_notes_entities', $record, $keys);
        }
        //Add new entity
        else {
          drupal_write_record('entity_notes_entities', $record);
        }
      }
    }
  }

  //Rebuild the menu to activate the new changes.
  menu_rebuild();

  drupal_set_message(t("The changes have been saved."));
}

function entity_notes_manage_note() {
  return ('Please use the Manage fields or Manage display to modify your notes.');
}